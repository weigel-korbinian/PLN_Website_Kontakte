import express from 'express';
import { v4 as uuidv4 } from 'uuid';

//-------------------
// Daten
//-------------------
const kontakte = [
    { id: '00', vorname: 'Anna', nachname: 'Arm', alter:'18', groesse:'1,10', beziehungsstatus:'single', lieblingsfarbe: 'rot', lieblingslehrer: 'kohnle' },
    { id: '11', vorname: 'Berta', nachname: 'Bein', alter:'18', groesse:'1,65', beziehungsstatus:'single', lieblingsfarbe: 'lavendel' , lieblingslehrer: 'kohnle' },
    { id: '22', vorname: 'Carla', nachname: 'Copf', alter:'18', groesse:'2,45', beziehungsstatus:'single', lieblingsfarbe: 'hotpink' , lieblingslehrer: 'kohnle' }
];

//-------------------
// Webserver
//-------------------
const app = express();
app.use(express.json()); // Für Body im POST

//---------------------------------------
// Webserver für statische Dateien GET /
//---------------------------------------
app.use(express.static('public'));

//-------------------
// API-Endpoints
//-------------------
//-------------------
// GET /kontakte/
//-------------------
app.get('/kontakte/', (request, response) => {
    response.send(kontakte);
});

//-------------------
// GET /kontakte/{id}
//-------------------
app.get('/kontakte/:id', (request, response) => {
    const id = request.params.id; // string
    const kontakt = kontakte.find(k => k.id === id);
    if (kontakt) {
        response.send(kontakt);
    } else {
        // Kein entsprechender Kontakt vorhanden
        response.status(404);
        response.send(`Es gibt keinen Kontakt mit id ${id}.`);
    }
});

//----------------------
// DELETE /kontakte/{id}
//----------------------
app.delete('/kontakte/:id', (request, response) => {
    const id = request.params.id; // string
    const index = kontakte.findIndex(k => k.id === id);
    if (index >= 0) {
        // kontakt Löschen
        kontakte.splice(index, 1);
        response.send(`Kontakt mit id ${id} wurde gelöscht.`);
    } else {
        // Kein entsprechender Kontakt vorhanden
        response.status(404);
        response.send(`Es gibt keinen Kontakt mit id ${id}.`);
    }
});

//-------------------
// POST /kontakte/
//-------------------
app.post('/kontakte/', (request, response) => {
    // neue Kontaktdaten aus Request-Body lesen
    const neuerKontakt = request.body;

    // FEHLT: Validierung !!!

    // neue id hinzufügen
    neuerKontakt.id = uuidv4();

    // neuen Kontakt zur Liste hinzufügen
    kontakte.push(neuerKontakt);

    // 200 OK zurücksschicken
    response.send('Es wurde ein neuer Kontakt erstellt.');
});

//-------------------
// PUT /kontakte/{id}
//-------------------
app.put('/kontakte/:id', (request, response) => {
    // id auswerten
    const id = request.params.id; // string

    // neue Kontaktdaten aus Request-Body lesen
    const veraenderterKontakt = request.body;

    const index = kontakte.findIndex(k => k.id === id);
    if (index >= 0) {
        // Daten ändern
        kontakte[index].vorname = veraenderterKontakt.vorname;
        kontakte[index].nachname = veraenderterKontakt.nachname;
        kontakte[index].alter = veraenderterKontakt.alter;
        kontakte[index].groesse = veraenderterKontakt.groesse;
        kontakte[index].beziehungsstatus = veraenderterKontakt.beziehungsstatus;
        kontakte[index].lieblingsfarbe = veraenderterKontakt.lieblingsfarbe;
        kontakte[index].lieblingslehrer = veraenderterKontakt.lieblingslehrer;
        
        // 200 OK zurücksschicken
        response.send('Es wurde ein Kontakt verändert.');
    } else {
        // Kein entsprechender Kontakt vorhanden
        response.status(404);
        response.send(`Es gibt keinen Kontakt mit id ${id}.`);
    }
});

//-------------------
// Server starten
//-------------------
app.listen(3000, () => {
    console.log('Server läuft auf Port 3000.');
});