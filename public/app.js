// Häckchen: ✓
const { createApp } = Vue;
createApp(
    {
        // Variablen
        data() {
            return {
                kontakte: [],
                main: {
                    visible: true
                },                
                modalNew: {
                    visible: false,
                    vorname: '',
                    nachname: '',
                    alter: '',
                    groesse: '',
                    beziehungsstatus: '',
                    lieblingsfarbe: '',
                    lieblingslehrer: ''
                },
                modalUpdate: {
                    visible: false,
                    id: '',
                    vorname: '',
                    nachname: '',
                    alter: '',
                    groesse: '',
                    beziehungsstatus: '',
                    lieblingsfarbe: '',
                    lieblingslehrer: ''
                },
                modalDelete: {
                    visible: false,
                    id: '',
                    vorname: '',
                    nachname: ''
                }
            }
        },

        // Methoden
        methods: {
            // Buttons
            buttonNewKontaktClick() {
                this.modalNew.vorname = 'Max';
                this.modalNew.nachname = 'Mustermann';
                this.modalNew.alter = '0';
                this.modalNew.groesse = '1,75';
                this.modalNew.beziehungsstatus = 'Single';
                this.modalNew.lieblingsfarbe = 'hotpink';
                this.modalNew.lieblingslehrer = 'hohaus';
                this.modalNew.visible = true;
                this.main.visible = false;
            },

            buttonNewCancelClick() {
                this.modalNew.visible = false;
                this.main.visible = true;
            },

            buttonNewSaveClick() {
                const newKontakt = {
                    vorname: this.modalNew.vorname,
                    nachname: this.modalNew.nachname,
                    alter: this.modalNew.alter,
                    groesse: this.modalNew.groesse,
                    beziehungsstatus: this.modalNew.beziehungsstatus,
                    lieblingsfarbe: this.modalNew.lieblingsfarbe,
                    lieblingslehrer: this.modalNew.lieblingslehrer
                };
                this.addKontakt(newKontakt);
                this.getAllKontakte();

                this.modalNew.visible = false;
                this.main.visible = true;
            },

            buttonUpdateClick(kontakt){
                // Daten übergeben
                this.modalUpdate.id = kontakt.id;
                this.modalUpdate.vorname = kontakt.vorname;
                this.modalUpdate.nachname = kontakt.nachname;
                this.modalUpdate.alter = kontakt.alter;
                this.modalUpdate.groesse = kontakt.groesse;
                this.modalUpdate.beziehungsstatus = kontakt.beziehungsstatus;
                this.modalUpdate.lieblingsfarbe = kontakt.lieblingsfarbe;
                this.modalUpdate.lieblingslehrer = kontakt.lieblingslehrer;

                // modalUpdate anzeigen
                this.modalUpdate.visible = true;
                this.main.visible = false;
            },

            buttonUpdateCancelClick() {
                // main anzeigen
                this.modalUpdate.visible = false;
                this.main.visible = true;
            },

            buttonUpdateSaveClick() {
                // update
                this.updateKontakt({
                    id: this.modalUpdate.id,
                    vorname: this.modalUpdate.vorname,
                    nachname: this.modalUpdate.nachname,
                    alter: this.modalUpdate.alter,
                    groesse: this.modalUpdate.groesse,
                    beziehungsstatus: this.modalUpdate.beziehungsstatus,
                    lieblingsfarbe: this.modalUpdate.lieblingsfarbe,
                    lieblingslehrer: this.modalUpdate.lieblingslehrer
                });
                this.getAllKontakte();

                // main anzeigen
                this.modalUpdate.visible = false;
                this.main.visible = true;
            },

            buttonDeleteClick(kontakt) {
                // Daten des zu löschenden Kontakts übergeben
                this.modalDelete.id = kontakt.id;
                this.modalDelete.vorname = kontakt.vorname;
                this.modalDelete.nachname = kontakt.nachname;
                this.modalDelete.alter = kontakt.alter;
                this.modalDelete.groesse = kontakt.groesse;
                this.modalDelete.beziehungsstatus = kontakt.beziehungsstatus;
                this.modalDelete.lieblingsfarbe = kontakt.lieblingsfarbe;
                this.modalDelete.lieblingslehrer = kontakt.lieblingslehrer;

                // modalDelete anzeigen
                this.modalDelete.visible = true;
                this.main.visible = false;                
            },

            buttonDeleteCancelClick(){
                // main anzeigen
                this.modalDelete.visible = false;
                this.main.visible = true;
            },

            buttonDeleteConfirmClick(){
                // Kontakt löschen
                this.deleteKontakt(this.modalDelete.id);
                this.getAllKontakte();

                // main anzeigen
                this.modalDelete.visible = false;
                this.main.visible = true;
            },

            // REST
            getAllKontakte() {
                fetch('/kontakte/') // GET
                    .then(response => response.json())
                    .then(data => this.kontakte = data);
            },

            addKontakt(kontakt) {
                fetch('/kontakte/', { // POST
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(kontakt)
                });
            },

            updateKontakt(kontakt){
                fetch(`/kontakte/${kontakt.id}`, { // PUT
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(kontakt)
                });
            },

            deleteKontakt(id) {
                fetch(`/kontakte/${id}`, { // DELETE
                    method: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
            },

            // UTILITIES
            titleCase(string){
                return string[0].toUpperCase() + string.slice(1).toLowerCase();
            },
            durchschnittsalter(){
                let summe = 0;
                for (let i = 0; i < this.kontakte.length; i++) {
                    summe += parseInt(this.kontakte[i].alter);
                }
                return parseFloat( summe / this.kontakte.length).toFixed(2);
            }
        },

        // Start
        mounted() {
            this.getAllKontakte();
        }
    }
).mount('#app');